//
//  SettingsViewModelTests.swift
//  SwitzerlandWeatherTests
//
//  Created by Ferran Alvarez Morales on 26/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import XCTest
@testable import SwitzerlandWeather

class SettingsViewModelTests: XCTestCase {

    private var settingsViewModel: SettingsViewModel!
    var userDefaults: UserDefaults!
    override func setUp() {
        super.setUp()
        self.settingsViewModel = SettingsViewModel()
        userDefaults = UserDefaults.standard
    }
    
    func test_should_return_correct_display_name_for_fahrenheit() {
        XCTAssertEqual(self.settingsViewModel.selectedUnit.displayName, "Fahrenheit")
    }
    func test_should_make_sure_that_default_selected_unit_is_fahrenheit() {
        XCTAssertEqual(settingsViewModel.selectedUnit, .fahrenheit)
    }
    
    func test_should_be_able_to_save_user_unit_selection() {
        self.settingsViewModel.selectedUnit = .celsius
        XCTAssertNotNil(userDefaults.value(forKey:"unit"))
    }
    
    override func tearDown() {
        userDefaults.removeObject(forKey: "unit")
    }

}
