//
//  WeatherListViewModelTest.swift
//  SwitzerlandWeatherTests
//
//  Created by Ferran Alvarez Morales on 26/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import XCTest

@testable import SwitzerlandWeather

class WeatherListViewModelTest: XCTestCase {

    private var weatherListViewModel : WeatherListViewModel!

    override func setUp() {
        super.setUp()
        self.weatherListViewModel = WeatherListViewModel()
        self.weatherListViewModel.addWeatherViewModel(WeatherViewModel(cityName: "Zurich", currentTemp: TemperatureViewModel(temp: 32, tempMin: 0, tempMax: 0)))
        self.weatherListViewModel.addWeatherViewModel(WeatherViewModel(cityName: "Bern", currentTemp: TemperatureViewModel(temp: 72, tempMin: 0, tempMax: 0)))
    }

    func test_should_be_able_to_convert_to_celsius_successfully() {
        
        let celsiusTemperatures = [0, 22.2222]
        self.weatherListViewModel.updateUnit(to: .celsius)
        
        for (index, viewModel) in self.weatherListViewModel.weatherViewModels.enumerated() {
            XCTAssertEqual(round(viewModel.currentTemperature.temperature.value), round(celsiusTemperatures[index]))
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }

}
