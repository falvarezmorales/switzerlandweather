//
//  Webservice.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 25/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation

struct Resource<T> {
    let url: URL
    let parse: (Data) -> T?
}

final class Webservice {
    func load<T>(resource:Resource<T>, completion: @escaping (T?) -> Void) {
        URLSession.shared.dataTask(with: resource.url) { data, response, error in
            print(resource.url)
            if let data = data {
                DispatchQueue.main.async { //UIView update
                    completion(resource.parse(data))
                }
            } else {
                completion(nil)
            }
            
        }.resume()
    }
}
