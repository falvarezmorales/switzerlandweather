//
//  Dynamic.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 26/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

class Dynamic<T>: Decodable where T: Decodable {
    
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    func bind(listener: @escaping Listener) {
        self.listener = listener
    }
    
    func bindAndFire(listener: @escaping Listener) {
        self.listener = listener
        self.listener?(self.value)
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    private enum CodingKeys: CodingKey {
        case value
    }
}
