//
//  AddWeatherCityViewModel.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 26/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation

struct AddCityViewModel {
    
    var city: String = ""
    var state: String = ""
    var zipCode: String = ""
}
