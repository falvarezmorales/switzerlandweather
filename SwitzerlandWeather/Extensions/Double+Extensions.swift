//
//  Double+Extensions.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 25/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation

extension Double {
    var formatAsDegree: String {
        return String(format: "%.0fº", self)
    }
}
