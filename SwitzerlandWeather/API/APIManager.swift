//
//  APIManager.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 27/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation

class APIManager {
    let APIEndpoint = "https://api.openweathermap.org/data/2.5/"
    let APIKey = "66e01ce4f8c230d2c55d3393dbd033d4" //You can get a free key registering on openweathermap.org, please, use your own key.
    
    func apiRequest<Element: Decodable>(_ url: URL, _ viewModelType: Element.Type, returnRes: @escaping (Decodable) -> Void) {
        
        let weatherResource = Resource<Decodable>(url: url) { data in
            let viewModel = try? JSONDecoder().decode(viewModelType, from: data)
            return viewModel
        }
        Webservice().load(resource: weatherResource) { result in
            if let viewModel = result {
                returnRes(viewModel)
            }
        }
    }
    
}

extension APIManager {
    func getCityWeather(city: String, returnRes: @escaping (Decodable) -> Void) {
        
        let weatherURL = URL(string: "\(APIEndpoint)weather?q=\(city)&APPID=\(APIKey)&units=imperial")!
        apiRequest(weatherURL, WeatherViewModel.self) { result in
                returnRes(result)
        }
    }
}
