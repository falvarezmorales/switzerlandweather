//
//  WeatherDetailsViewController.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 26/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation
import UIKit

class WeatherDetailsViewController: UIViewController {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    
    var weatherViewModel: WeatherViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupVMBindings()
        
    }
    
    private func setupVMBindings() {
        if let weatherVM = self.weatherViewModel {
            weatherVM.name.bindAndFire { self.cityNameLabel.text = $0 }
            weatherVM.currentTemperature.temperature.bindAndFire { self.currentTemperatureLabel.text = $0.formatAsDegree }
            
        }
    }
}
