//
//  AddWeatherCityViewController.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 24/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation
import UIKit

protocol AddWeatherDelegate : class {
    func addWeatherDidSave(viewModel: WeatherViewModel)
}

class AddWeatherCityViewController : UIViewController {
    
    private let apiManager = APIManager()
    private var addCityViewModel = AddCityViewModel()
    @IBOutlet weak var cityNameTextField: BindingTextField! {
        didSet {
            cityNameTextField.bind { self.addCityViewModel.city = $0 }
        }
    }
    @IBOutlet weak var stateTextField: BindingTextField! {
        didSet {
            stateTextField.bind { self.addCityViewModel.state = $0 }
        }
    }
    @IBOutlet weak var zipCodeTextField: BindingTextField! {
        didSet {
            zipCodeTextField.bind { self.addCityViewModel.zipCode = $0 }
        }
    }
    
    weak var delegate: AddWeatherDelegate?
    
    @IBAction func saveCityButtonPressed() {
        if let city = cityNameTextField.text {
            if let del = delegate {
                
                apiManager.getCityWeather(city: city) { [weak self] weatherVM in
                    del.addWeatherDidSave(viewModel: weatherVM as! WeatherViewModel)
                    self?.dismiss(animated: true, completion: nil)
                }
            }
            
        }
    }
    
    @IBAction func closeButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
}
