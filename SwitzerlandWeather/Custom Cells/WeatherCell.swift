//
//  WeatherCell.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 25/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation
import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    func configure(_ viewModel: WeatherViewModel) {
        self.cityNameLabel.text = viewModel.name.value
        self.temperatureLabel.text = "\(viewModel.currentTemperature.temperature.value.formatAsDegree)"
    }
}
